package tb.sockets.server;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Random;

public class Serwer {
	Socket sock;
	BufferedReader reader;
	OutputStream output;
	PrintWriter pwrite;
	InputStream input;
	ServerSocket ssock = new ServerSocket(6666);
	static BufferedReader breader;
	Random generator = new Random();
	int number1, number2;
	
	public Serwer() throws Exception
	{
		sock = ssock.accept();
		reader = new BufferedReader(new InputStreamReader(System.in));
		output = sock.getOutputStream(); 
		pwrite = new PrintWriter(output, true);
		input = sock.getInputStream();
		breader = new BufferedReader(new InputStreamReader(input));
	}

	public void Game() {
		pwrite.flush();
		number1 = generator.nextInt(3);
		number2 = generator.nextInt(3);
		MainFrame.SOAction(MainFrame.buttons[number1][number2]);
		pwrite.write("O");
		pwrite.flush();
	}
}
