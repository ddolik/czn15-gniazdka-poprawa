package tb.sockets.server;

import java.awt.event.ActionEvent;

import javax.swing.ImageIcon;
import javax.swing.JButton;

public class XOButton extends JButton {
	ImageIcon X = new ImageIcon(this.getClass().getResource("X.png"));
	ImageIcon O = new ImageIcon(this.getClass().getResource("O.png"));
	String ikona = new String();
	boolean clicked = false;
	
	public XOButton() {
		//addActionListener(this);
	}
	
	// ruch serwera
	public void OAction(XOButton b) {
		this.ikona = "O";
		this.clicked = true;
		this.setIcon(O);
	}
}
