package tb.sockets.client;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;

public class XOButton extends JButton implements ActionListener {
	
	ImageIcon X = new ImageIcon(this.getClass().getResource("X.png"));
	ImageIcon O = new ImageIcon(this.getClass().getResource("O.png"));
	String ikona = new String();
	boolean clicked = false;
	static boolean sth = false; // czy cokolwiek by�o naci�ni�te
	
	public XOButton() {
		addActionListener(this);
	}
	
	// co przy kliknieciu
	public void actionPerformed(ActionEvent e)
	{
		setIcon(X);
		this.ikona = "X";
		this.clicked = true;
		sth = true;
	}
	
	// ruch serwera
	public void OAction() {
		this.ikona = "O";
		this.clicked = true;
		this.setIcon(O);
	}
	
}
