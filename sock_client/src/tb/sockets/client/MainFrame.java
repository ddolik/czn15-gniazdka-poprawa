package tb.sockets.client;

import java.awt.EventQueue;
import java.text.FieldPosition;
import java.text.Format;
import java.text.ParseException;
import java.text.ParsePosition;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.text.MaskFormatter;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JFormattedTextField;
import java.awt.Color;
import java.awt.GridLayout;
import javax.swing.border.LineBorder;

public class MainFrame extends JFrame {
	
	private JPanel p = new JPanel();
	static XOButton buttons[][] = new XOButton[3][3];
	static boolean ifX = false; // czy wygral krzyzyk
	static boolean ifO = false; // czy wygralo kolko
	static boolean eog = false; // koniec gry
	static boolean remis = false; // remis
	
	/**
	 * Launch the application.
	 */
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainFrame frame = new MainFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MainFrame() {
		
		super("K�ko i krzy�yk");
		setLayout(new GridLayout(3,3));
		for(int i = 0; i < 3; i++)
			for (int j = 0; j < 3; j++)
			{
				buttons[i][j] = new XOButton();
				this.add(buttons[i][j]);
			}
		setSize(400,400);
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

	}
	
	public void OWin() // wygrywa kolko
	{
		if (ifO == true)
		{
			this.setEnabled(false);
			System.out.println("Wygralo kolko!");
			eog = true;
		}
	}
	
	public void XWin() // wygrywa krzyzyk
	{
		if (ifX == true)
		{
			this.setEnabled(false);
			System.out.println("Wygral krzyzyk!");
			eog = true;
		}
	}
	
	//spr czy wygrywa krzyzyk
	public void XCheck() {
		if ((buttons[1][1].ikona == "X" && buttons[1][2].ikona == "X" && buttons[1][3].ikona == "X")
				|| (buttons[2][1].ikona == "X" && buttons[2][2].ikona == "X" && buttons[2][3].ikona == "X")
				|| (buttons[3][1].ikona == "X" && buttons[3][2].ikona == "X" && buttons[3][3].ikona == "X")
				|| (buttons[1][1].ikona == "X" && buttons[2][1].ikona == "X" && buttons[3][1].ikona == "X")
				|| (buttons[1][2].ikona == "X" && buttons[2][2].ikona == "X" && buttons[3][2].ikona == "X")
				|| (buttons[1][3].ikona == "X" && buttons[2][3].ikona == "X" && buttons[3][3].ikona == "X")
				|| (buttons[1][1].ikona == "X" && buttons[2][2].ikona == "X" && buttons[3][3].ikona == "X")
				|| (buttons[1][3].ikona == "X" && buttons[2][2].ikona == "X" && buttons[3][1].ikona == "X"))
		{
			ifX = true;
			XWin();
		}
	}
	
	//spr czy wygrywa kolko
	public void OCheck() {
		if ((buttons[1][1].ikona == "O" && buttons[1][2].ikona == "O" && buttons[1][3].ikona == "O")
				|| (buttons[2][1].ikona == "O" && buttons[2][2].ikona == "O" && buttons[2][3].ikona == "O")
				|| (buttons[3][1].ikona == "O" && buttons[3][2].ikona == "O" && buttons[3][3].ikona == "O")
				|| (buttons[1][1].ikona == "O" && buttons[2][1].ikona == "O" && buttons[3][1].ikona == "O")
				|| (buttons[1][2].ikona == "O" && buttons[2][2].ikona == "O" && buttons[3][2].ikona == "O")
				|| (buttons[1][3].ikona == "O" && buttons[2][3].ikona == "O" && buttons[3][3].ikona == "O")
				|| (buttons[1][1].ikona == "O" && buttons[2][2].ikona == "O" && buttons[3][3].ikona == "O")
				|| (buttons[1][3].ikona == "O" && buttons[2][2].ikona == "O" && buttons[3][1].ikona == "O"))
		{
			ifO = true;
			OWin();
		}

	}
	
	// spr czy jest remis
	public static void XOCheck() {
		boolean czy = true;
		for (int i = 0; i < 3; i++)
			for (int j = 0; j < 3; j++)
				if (buttons[i][j].clicked == false) czy = false;
		if (czy)
		{
			eog = true;
			remis = true;
	//		this.setEnabled(false);
			System.out.println("Remis!");
		}
	}
	
	public static void SOAction(XOButton b) {
		b.OAction();
	//	OCheck();
	}
	
}